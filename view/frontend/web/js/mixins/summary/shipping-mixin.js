/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'underscore',
    'Magento_Checkout/js/view/summary/abstract-total',
    'Magento_Checkout/js/model/quote'
], function ($, _, Component, quote) {
    'use strict';

    return function (Component) {
        return Component.extend({
            /**
             * @return {*}
             */
            getShippingMethodTitle: function () {
                var shippingMethod,
                    shippingMethodTitle = '';

                if (!this.isCalculated()) {
                    return '';
                }
                shippingMethod = quote.shippingMethod();

                if (!_.isArray(shippingMethod) && !_.isObject(shippingMethod)) {
                    return '';
                }

                if (typeof shippingMethod['carrier_title'] === 'undefined') {
                    return shippingMethod['method_title'];
                }

                if (typeof shippingMethod['method_title'] !== 'undefined') {
                    shippingMethodTitle = ' - ' + shippingMethod['method_title'];
                }

                return shippingMethodTitle ?
                    shippingMethod['carrier_title'] + shippingMethodTitle :
                    shippingMethod['carrier_title'];
            }
        });
    }
});
